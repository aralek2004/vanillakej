// ****************************************************************
// Controller functions to get the requested data from the models, 
// create an HTML page displaying the data, and return it to the 
// user to view in the browser.
// ****************************************************************
import path from 'path';
const __dirname = path.resolve();
// show html page
export const home = (req, res) => {
    //show this file when the "/" is requested
    res.sendFile(__dirname+"/source/pages/home.html");
}
// get and show today's date
export const getTodayDate = (req, res) => {
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var newdate = day+ "/" + month + "/" + year;
    res.json({
        today: newdate
    });
}
// get list of month names
export const getMonthsName = (req, res) => {
    res.json({
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec'
    });
}
// get list of month names
export const getWeekDays = (req, res) => {
    res.json({
        1: 'Mon',
        2: 'Tue',
        3: 'Wed',
        4: 'Thur',
        5: 'Fri',
        6: 'Sat',
        7: 'Sun',
    });
}

// get list of people -- This can come from a database and what's defined in model.js
// but for the purpose of this demo, I'll create nested dictionaries
export const getPeople = (req, res) => {
    res.json([
        {
            FirstName: 'Keji',
            LastName: 'Araloyin',
            title: 'Engineer'
        },
        {
            FirstName: 'Bob',
            LastName: 'Gary',
            title: 'Engineer'
        },
        {
            FirstName: 'Kerry',
            LastName: 'Miller',
            title: 'Engineer'
        },
        {
            FirstName: 'Peter',
            LastName: 'Joy',
            title: 'Engineer'
        }
    ]);
}